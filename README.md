ics-ans-sonarqube
===================

Ansible playbook to install SonarQube.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.20

License
-------

BSD 2-clause
